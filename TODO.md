# TODO

- Fix toggle blockquote
- Toggle paragraph
  - Add button to change into paragraph
  - Disable Heading button in Menu and HoveringToolbar (see TODOs there)
  - Disable List button in Menu and HoveringToolbar (see TODOs there)
- Turn P/H1/H2 into a select
- Research adding YOAST readability scores


- Strapi v4 uses `styled-components`. Replace `emotion` with same version to avoid extra dependency
- Insert images