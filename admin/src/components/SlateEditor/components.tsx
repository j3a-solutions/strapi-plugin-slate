/** @jsx jsx */

import * as React from 'react';

import ReactDOM from 'react-dom';

import { jsx } from '@emotion/react';
import { Status } from '@strapi/design-system';

export const BodyPortal: React.FC<PropsWithChildren> = ({ children }) => {
  return typeof document === 'object'
    ? ReactDOM.createPortal(children as any, document.body)
    : null;
};

export const Trim: React.FC<PropsWithChildren & { size?: string }> = ({
  children,
  size = '200px'
}) => {
  return (
    <span
      css={{
        textOverflow: 'ellipsis',
        overflow: 'hidden',
        whiteSpace: 'nowrap',
        maxWidth: size
      }}
    >
      {children}
    </span>
  );
};

export const FieldErrorMessage: React.FC<{ message: string }> = ({
  message
}) => (
  <Status className="d-block" variant="danger">
    {message}
  </Status>
);
