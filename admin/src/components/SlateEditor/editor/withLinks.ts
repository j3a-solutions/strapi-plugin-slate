import { Editor } from 'slate';
import isUrl from 'is-url';
import { insertExternalLink, isLinkElement } from '../elements/link';

export const withLinks = (editor: Editor) => {
  const { insertData, insertText, isInline } = editor;

  editor.isInline = element => {
    return isLinkElement(element) ? true : isInline(element);
  };

  editor.insertText = text => {
    if (text && isUrl(text)) {
      /**
       * Transform link into ExternalLinkElement element
       */
      insertExternalLink(editor, text);
    } else {
      /**
       * Default to editor's insertText
       */
      insertText(text);
    }
  };

  editor.insertData = data => {
    const text = data.getData('text/plain');

    if (text && isUrl(text)) {
      insertExternalLink(editor, text);
    } else {
      insertData(data);
    }
  };

  return editor;
};
