import { CustomElement, CustomText, ElementType } from '../elements/types';
import { Descendant, Text, createEditor } from 'slate';
import { HoveringToolbar, StaticToolbar } from '../menu/index';
import { LinkHoveringToolbar } from '../menu/link';
import React, { useMemo, useState } from 'react';
import { Slate, withReact } from 'slate-react';

import { EditableContent } from '../editable';
import { hyphenate } from 'hyphen/en';
import { withHistory } from 'slate-history';
import { withLinks } from './withLinks';

interface SlateEditorProps {
  initialValue: Descendant[];
  onChange: (x: Descendant[]) => void;
}

const isEmptyValue = (x: Descendant[]) => {
  const first = x[0] as CustomElement;
  return (
    x.length === 1 &&
    first.t === ElementType.Paragraph &&
    first.children.length === 1 &&
    (first.children[0] as CustomText).text === ''
  );
};

async function parseLeaves(value: Descendant[]): Promise<Descendant[]> {
  return Promise.all(
    value.flatMap(async x => {
      if (Text.isText(x)) {
        return {
          ...x,
          text: await hyphenate(x.text)
        } as CustomText;
      }

      return {
        ...x,
        children: await parseLeaves(x.children)
      } as CustomElement;
    })
  );
}

const SlateEditor: React.FC<SlateEditorProps> = React.memo(
  ({ initialValue, onChange }) => {
    /**
     * Keep the same editor instance when a new value is set
     */
    const editor = useMemo(
      () => withLinks(withHistory(withReact(createEditor()))),
      []
    );
    const [value, setValue] = useState<Descendant[]>(initialValue);

    /**
     * Strapi Rich Content value may be null or a string
     * When it's first rendered initial value will always be null
     * If there is a value, Strapi will push it down on a second render
     *
     * Slate can't function with null or empty string, so it should get EMPTY_VALUE to start with always
     *
     * Later on, the editor state should be updated only ONCE
     * when initialValue goes from EMPTY_VALUE to something with content
     */
    const isEmptyVeryFirst = useMemo(() => isEmptyValue(initialValue), []);
    const [isEmpty, setIsEmpty] = useState(isEmptyVeryFirst);

    if (isEmpty && !isEmptyValue(initialValue)) {
      setIsEmpty(false);
      setValue(initialValue);
    }

    return (
      <Slate
        editor={editor}
        value={value}
        onChange={value => {
          // console.log('dispatching new value: ', value);
          setValue(value);
          parseLeaves(value).then(onChange);
        }}
      >
        <LinkHoveringToolbar />
        <StaticToolbar />
        <HoveringToolbar />
        <EditableContent />
      </Slate>
    );
  },
  (prev, next) => {
    const isPrevEmpty = isEmptyValue(prev.initialValue);
    const isNextEmpty = isEmptyValue(next.initialValue);
    const hasAlreadyLoadedValueWithContent = !isPrevEmpty;
    const hasJustLoadedContent = isPrevEmpty && !isNextEmpty;
    return hasAlreadyLoadedValueWithContent || !hasJustLoadedContent;
  }
);

export default SlateEditor;
