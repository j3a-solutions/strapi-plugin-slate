/** @jsx jsx */

import * as React from 'react';

import {
  Editable,
  RenderElementProps,
  RenderLeafProps,
  useSlateStatic
} from 'slate-react';
import {
  ElementType,
  findElementInSelection,
  FormattedText,
  Mark,
  selectionContains,
  toggleList
} from './elements';
import { assertUnreachable } from './helpers';
import { Typography, Link } from '@strapi/design-system';

import { Transforms } from 'slate';
import isHotkey from 'is-hotkey';
import { jsx } from '@emotion/react';
import { toggleMark } from './format';
import { useFocused } from 'slate-react';
import styled from '@emotion/styled';
import { emptyParagraph } from './elements/paragraph';

const HOTKEYS: Record<string, Mark> = {
  'mod+b': 'bold',
  'mod+i': 'italic',
  'mod+u': 'underline'
  // 'mod+`': 'code'
};

const H2 = styled.h2`
  font-size: 1.6rem;
  font-weight: bold;
  margin-bottom: 1.6rem;
  margin-left: 0.8rem;
`;

const H3 = styled.h3`
  font-size: 1.3rem;
  font-weight: semibold;
  margin-bottom: 1.3rem;
  margin-left: 1.6rem;
`;

const UnorderedList = styled.ul`
  list-style-type: disc;
  margin-left: 2.5rem;
  margin-bottom: 1rem;
`;

const OrderedList = styled.ol`
  list-style-type: decimal;
  margin-left: 2.5rem;
  margin-bottom: 1rem;
`;
const ListItem = styled.li``;

const BlockQuote = styled.blockquote`
  margin-top: 10px;
  margin-bottom: 10px;
  margin-left: 10px;
  padding-left: 15px;
  border-left: 3px solid #ccc;
`;

const LinkWithRef = React.forwardRef(
  (props: RenderElementProps & { href: string }, ref: any) => (
    <span {...props.attributes} ref={ref}>
      <Link href={props.href}>
        <span css={{ fontSize: '14px' }}>{props.children}</span>
      </Link>
    </span>
  )
);

const HTMLElement: React.FC<RenderElementProps> = props => {
  const { attributes, children, element } = props;
  switch (element.t) {
    case ElementType.BlockQuote:
      return (
        <Typography as={BlockQuote} {...attributes}>
          {children}
        </Typography>
      );
    case ElementType.Heading:
      switch (element.level) {
        case 2:
          return <H2>{children}</H2>;
        case 3:
          return <H3>{children}</H3>;
        default:
          assertUnreachable(element.level);
      }

    case ElementType.List:
      return element.ordered ? (
        <OrderedList {...attributes}>{children}</OrderedList>
      ) : (
        <UnorderedList {...attributes}>{children}</UnorderedList>
      );
    case ElementType.ListItem:
      return (
        <Typography as={ListItem} {...attributes}>
          {children}
        </Typography>
      );
    case ElementType.LinkExternal:
      return (
        <LinkWithRef {...props} href={element.url}>
          {children}
        </LinkWithRef>
      );
    case ElementType.LinkInternal:
      return (
        <a
          {...attributes}
          href={`/admin/content-manager/collectionType/${element.contentName}/${element.contentId}`}
        >
          {children}
        </a>
      );
    case ElementType.Paragraph:
      return (
        <div
          css={{
            marginBottom: '1rem'
          }}
        >
          <Typography as="p" {...attributes}>
            {children}
          </Typography>
        </div>
      );
    default:
      assertUnreachable(element);
  }
};

const Leaf: React.FC<RenderLeafProps> = ({ attributes, children, leaf }) => {
  if (leaf.bold) {
    children = <strong css={{ fontWeight: 'bold' }}>{children}</strong>;
  }

  if (leaf.italic) {
    children = <em css={{ fontStyle: 'italic' }}>{children}</em>;
  }

  if (leaf.underline) {
    children = <u css={{ textDecoration: 'underline' }}>{children}</u>;
  }

  return <span {...attributes}>{children}</span>;
};

const renderElement = (props: RenderElementProps) => <HTMLElement {...props} />;
const renderLeaf = (props: RenderLeafProps) => <Leaf {...props} />;

export const EditableContent: React.FC = () => {
  const editor = useSlateStatic();
  const focused = useFocused();

  return (
    <Editable
      renderElement={renderElement}
      renderLeaf={renderLeaf}
      spellCheck
      css={{
        border: `1px solid ${focused ? '#78caff' : '#E3E9F3'}`,
        borderRadius: '2px',
        padding: '1rem'
      }}
      onKeyDown={event => {
        /**
         * Exit from Heading
         */
        if (
          (event.key === 'Enter' || event.keyCode === 13) &&
          selectionContains(editor, ElementType.Heading)
        ) {
          event.preventDefault();
          Transforms.insertNodes(editor, emptyParagraph());
          return;
        }

        /**
         * Exit from list
         */
        if (
          event.key === 'Enter' &&
          selectionContains(editor, ElementType.List)
        ) {
          const listItem = findElementInSelection(
            editor,
            ElementType.ListItem
          )?.[0];

          if ((listItem?.children as FormattedText[])[0].text === '') {
            // Handle double Enter keying
            event.preventDefault();
            toggleList(editor, false);
          }
          return;
        }

        for (const hotkey in HOTKEYS) {
          if (isHotkey(hotkey, event.nativeEvent)) {
            event.preventDefault();
            const mark = HOTKEYS[hotkey];
            toggleMark(editor, mark);
            return;
          }
        }

        if (event.key === 'Enter') {
          event.preventDefault();
          Transforms.insertNodes(editor, emptyParagraph());
          return;
        }
      }}
    />
  );
};
