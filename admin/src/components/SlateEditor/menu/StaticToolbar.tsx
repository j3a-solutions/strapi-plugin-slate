import {
  BlockquoteLeft,
  ListOl,
  ListUl,
  TypeBold,
  TypeH2,
  TypeH3,
  TypeItalic,
  TypeUnderline
} from 'react-bootstrap-icons';
import { MarkButton, VerticalSeparator } from './buttons';
import React from 'react';
import { ReactEditor, useSlate } from 'slate-react';

import { HeadingButton } from './Heading';
import { LinkButton } from './link';
import { ListButton } from './List';
import { Flex, IconButtonGroup } from '@strapi/design-system';
import { BlockQuoteButton } from './Blockquote';
import styled from '@emotion/styled';

const Container = styled.div`
  margin: 1rem 0;
`;

export const StaticToolbar: React.FC = () => {
  const editor = useSlate();

  /**
   * Fix lose of focus when using Toolbar buttons
   * https://github.com/ianstormtaylor/slate/issues/3412
   */
  const divRef = React.useRef<HTMLDivElement>(null);
  const focusEditor = (e: React.MouseEvent) => {
    if (e.target === divRef.current) {
      ReactEditor.focus(editor);
      e.preventDefault();
    }
  };

  return (
    <Container>
      <Flex ref={divRef} onMouseDown={focusEditor}>
        <IconButtonGroup>
          <MarkButton mark="bold" icon={<TypeBold size={20} />} />
          <MarkButton mark="italic" icon={<TypeItalic size={20} />} />
          <MarkButton mark="underline" icon={<TypeUnderline size={20} />} />
        </IconButtonGroup>

        <VerticalSeparator />

        <IconButtonGroup>
          <HeadingButton level={2} icon={<TypeH2 size={20} />} />
          <HeadingButton level={3} icon={<TypeH3 size={20} />} />
        </IconButtonGroup>

        <VerticalSeparator />

        <IconButtonGroup>
          <ListButton ordered icon={<ListOl size={20} />} />
          <ListButton icon={<ListUl size={20} />} />
        </IconButtonGroup>

        <VerticalSeparator />

        <IconButtonGroup>
          <LinkButton />
          <BlockQuoteButton icon={<BlockquoteLeft size={20} />} />
        </IconButtonGroup>
      </Flex>
    </Container>
  );
};
