/** @jsx jsx */

import * as Yup from 'yup';

import { BaseSelection, Transforms } from 'slate';
import { BodyPortal, FieldErrorMessage, Trim } from '../components';
import { ElementType } from '../elements/types';
import { Form, Field, FormRenderProps, useForm } from 'react-final-form';
import { Link as LinkIcon, Trash } from 'react-bootstrap-icons';
import React, { useEffect, useRef, useState } from 'react';
import { ReactEditor, useSlate } from 'slate-react';
import { jsx } from '@emotion/react';
import { Status } from '@strapi/design-system';

import { Menu } from '.';
import { ToolbarButton } from './buttons';
import { request } from '@strapi/helper-plugin';
import { useSelector } from 'react-redux';
import {
  Button,
  TextInput,
  Loader,
  ModalLayout,
  ModalBody,
  ModalHeader,
  ModalFooter,
  Typography,
  Combobox,
  ComboboxOption
} from '@strapi/design-system';
import {
  activeExternalLink,
  activeInternalLink,
  insertExternalLink,
  insertInternalLink,
  isExternalLink,
  isLinkActive,
  unwrapLink
} from '../elements/link';
import { useSelectionCanHaveInline } from '../hooks';

const LinkIconAny = LinkIcon as any;
const TrashAny = Trash as any;

// TODO: this is too much info for the Slate Node
interface LinkOptionValues {
  externalUrl: string;
  contentName: string;
  internalId: string;
  internalLabel: string;
}

const selectModels = (state: any): any[] =>
  state['content-manager_app']?.models || [];

const apiIdToEndpoint = (id: string) =>
  `/content-manager/collection-types/${id}`;

type InternalLinkOption = {
  label: string;
  value: string;
};

const DocumentSelect: React.FC<{ values: LinkOptionValues }> = ({ values }) => {
  const form = useForm();
  const apiId = values.contentName;
  const [options, setOptions] = useState<InternalLinkOption[] | null>(null);

  useEffect(() => {
    const loadData = async () => {
      if (!apiId) {
        return setOptions(null);
      }

      try {
        const data = await request(`${apiIdToEndpoint(apiId)}?pageSize=200`);

        const options: InternalLinkOption[] = data.results.map((x: any) => {
          const label = (
            x.name ||
            x.title ||
            x.pageMetadata?.title ||
            'Anonymous'
          ).toString();

          return {
            value: x.id.toString(),
            label
          } as InternalLinkOption;
        });

        if (options.length) {
          setOptions(options);
        }
      } catch (error) {
        console.error(error);
        console.error(
          `Unable to get Strapi data. Does the endpoint match API ID of ${apiId}?`
        );
      }
    };

    loadData();
  }, [apiId]);

  const handleChange = (value: string) => {
    const label = options?.find(x => x.value == value)?.label;

    if (!label) {
      console.error('Unable to create internal link without label: ', value);
      throw Error('Unable to create internal link');
    }

    form.batch(() => {
      form.change('internalId', value);
      form.change('internalLabel', label);
    });
  };

  return (
    <div css={{ marginTop: 16 }}>
      {options ? (
        <React.Fragment>
          <Combobox
            label="Document"
            value={values.internalId}
            onChange={handleChange}
          >
            {options.map(o => (
              <ComboboxOption key={o.value} value={o.value}>
                {o.label}
              </ComboboxOption>
            ))}
          </Combobox>
          {/* <FieldErrorMessage message="internalId" /> */}
        </React.Fragment>
      ) : (
        <Loader>Loading documents...</Loader>
      )}
    </div>
  );
};

// TODO: move this to server config
const ALLOWED_INTERNAL_CONTENT_FOR_LINKS = [
  'blog-post',
  'contact-us-page',
  'ecofesto',
  'home-page',
  'ingredient',
  'policy-page',
  'press-contact-page',
  'product',
  'product-category'
];

const LinkForm: React.FC<{ values: LinkOptionValues }> = ({ values }) => {
  const models: Array<React.ReactElement> = useSelector<any, any[]>(
    selectModels
  ).reduce((acc: Array<React.ReactElement>, model) => {
    const { apiID, info, uid } = model;
    if (
      /^api::/.test(uid) &&
      ALLOWED_INTERNAL_CONTENT_FOR_LINKS.includes(apiID)
    ) {
      acc.push(
        <ComboboxOption key={apiID} value={uid}>
          {info.displayName}
        </ComboboxOption>
      );
    }

    return acc;
  }, []);

  return (
    <React.Fragment>
      <section>
        <h5
          css={{
            fontSize: '18px',
            marginBottom: '16px'
          }}
        >
          External Link
        </h5>
        <div>
          <Field
            name="externalUrl"
            render={({ input, meta }) => {
              return (
                <React.Fragment>
                  <TextInput
                    label="External Url"
                    placeholder="https://example.com"
                    {...input}
                  />
                  {meta.touched && meta.error && (
                    <Status className="d-block" variant="danger">
                      {meta.error}
                    </Status>
                  )}
                </React.Fragment>
              );
            }}
          />
        </div>
      </section>
      <section css={{ marginTop: 24 }}>
        <h5
          css={{
            fontSize: '18px',
            marginBottom: '16px'
          }}
        >
          Internal Link
        </h5>
        <div>
          <Field
            name="contentName"
            render={({ input, meta }) => {
              return (
                <React.Fragment>
                  <Combobox label="Content Type" {...input}>
                    {models}
                  </Combobox>
                  {meta.touched && meta.error && (
                    <Status className="d-block" variant="danger">
                      {meta.error}
                    </Status>
                  )}
                </React.Fragment>
              );
            }}
          />
        </div>
      </section>
      {values.contentName && <DocumentSelect values={values} />}
    </React.Fragment>
  );
};

export const LinkButton: React.FC<{ dark?: boolean }> = ({ dark }) => {
  const editor = useSlate();
  const [show, setShow] = useState(false);
  const [prevSelection, setPrevSelection] = useState<BaseSelection>(null);
  const selectionCanHaveInline = useSelectionCanHaveInline();

  const hideForm = () => {
    setShow(false);
    ReactEditor.focus(editor);
    if (prevSelection) {
      Transforms.select(editor, prevSelection);
    }
  };

  const handleShow = () => {
    setPrevSelection(editor.selection);
    setShow(true);
  };

  return (
    <React.Fragment>
      <ToolbarButton
        dark={dark}
        active={isExternalLink(editor)}
        disabled={!selectionCanHaveInline}
        onClick={handleShow}
        icon={<LinkIconAny size={20} />}
      />

      {show && (
        <Form
          initialValues={
            {
              externalUrl: '',
              contentName: '',
              internalId: '',
              internalLabel: ''
            } as LinkOptionValues
          }
          validateOnChange={false}
          validationSchema={Yup.object({
            url: Yup.string().url()
          })}
          onSubmit={({
            externalUrl,
            contentName,
            internalId,
            internalLabel
          }) => {
            setShow(false);
            ReactEditor.focus(editor);

            if (prevSelection) {
              Transforms.select(editor, prevSelection);
              if (externalUrl) {
                insertExternalLink(editor, externalUrl);
              } else if (contentName && internalId) {
                insertInternalLink(editor, {
                  contentName,
                  contentId: +internalId,
                  label: internalLabel
                });
              }
            }
          }}
        >
          {(form: FormRenderProps<LinkOptionValues, LinkOptionValues>) => (
            <ModalLayout onClose={() => {}} labelledBy="title">
              <ModalHeader>
                <Typography
                  fontWeight="bold"
                  textColor="neutral800"
                  as="h2"
                  id="title"
                >
                  Link Options
                </Typography>
              </ModalHeader>
              <ModalBody>
                <LinkForm values={form.values} />
              </ModalBody>
              <ModalFooter
                startActions={
                  <Button onClick={hideForm} variant="tertiary">
                    Cancel
                  </Button>
                }
                endActions={
                  <Button onClick={form.handleSubmit} type="submit">
                    <span css={{ color: 'white' }}>Apply</span>
                  </Button>
                }
              />
            </ModalLayout>
          )}
        </Form>
      )}
    </React.Fragment>
  );
};

const LinkMenu = () => {
  const editor = useSlate();
  const models = useSelector<any, any[]>(selectModels);

  const node = activeExternalLink(editor) || activeInternalLink(editor);

  if (!node) {
    return null;
  }

  const [link] = node;

  let href: string;
  let text: string;
  let target: string;

  if (link.t === ElementType.LinkExternal) {
    href = link.url;
    text = link.url;
    target = '_blank';
  } else {
    const model = models.find(x => x.uid === link.contentName);
    if (!model) {
      console.error('Unable to find model for contentName: ', link.contentName);
      href = '';
    } else {
      href = `/admin/content-manager/collectionType/${model.uid}/${link.contentId}`;
    }

    text = link.label;
    target = '_self'; // strapi would ask to login and never redirect to content type page
  }

  return (
    <Menu>
      <a
        href={href}
        target={target}
        css={{
          color: 'white',
          display: 'flex',
          alignItems: 'center'
        }}
      >
        <LinkIconAny css={{ marginRight: '5px' }} size={14} />
        <Trim>{text}</Trim>
      </a>
      <TrashAny
        onMouseDown={(e: React.MouseEvent) => {
          ReactEditor.focus(editor);
          e.preventDefault();
          unwrapLink(editor);
        }}
        css={{ cursor: 'pointer', color: '#bd2130' }}
        size={20}
      />
    </Menu>
  );
};

export const LinkHoveringToolbar = () => {
  const ref = useRef<HTMLDivElement>(null);
  const editor = useSlate();

  useEffect(() => {
    const el = ref.current;

    if (!el) {
      return;
    }

    if (
      !editor.selection ||
      !ReactEditor.isFocused(editor) ||
      !isLinkActive(editor)
    ) {
      el.removeAttribute('style');
      return;
    }

    const domSelection = window.getSelection();
    if (domSelection) {
      const rect = domSelection.getRangeAt(0).getBoundingClientRect();
      el.style.opacity = '1';
      el.style.top = `${rect.top + window.pageYOffset - el.offsetHeight}px`;
      el.style.left = `${
        rect.left + window.pageXOffset - el.offsetWidth / 2 + rect.width / 2
      }px`;
    }
  });

  return (
    <BodyPortal>
      <div
        ref={ref}
        css={{
          padding: '8px 7px 6px',
          position: 'absolute',
          zIndex: 1,
          top: '-10000px',
          left: '-10000px',
          marginTop: '-6px',
          opacity: 0,
          backgroundColor: '#18202e',
          borderRadius: '4px',
          transition: 'opacity 0.75s',
          color: 'white'
        }}
      >
        <LinkMenu />
      </div>
    </BodyPortal>
  );
};
