/** @jsx jsx */

import {
  ListOl,
  ListUl,
  TypeBold,
  TypeH2,
  TypeH3,
  TypeItalic,
  TypeUnderline
} from 'react-bootstrap-icons';
import { Editor } from 'slate';
import { MarkButton, VerticalSeparator } from './buttons';
import React, { useEffect, useRef } from 'react';
import { ReactEditor, useSlate } from 'slate-react';
import { HeadingButton } from './Heading';
import { LinkButton } from './link';
import { ListButton } from './List';
import ReactDOM from 'react-dom';
import { jsx } from '@emotion/react';
import styled from '@emotion/styled';
import { IconButtonGroup } from '@strapi/design-system';
import { useSelectionCanHaveInline } from '../hooks';

export const Menu = styled.div`
  display: grid;
  column-gap: 15px;
  grid-auto-flow: column;
`;

const Portal: React.FC<PropsWithChildren> = ({ children }) => {
  return typeof document === 'object'
    ? ReactDOM.createPortal(children as any, document.body)
    : null;
};

export const HoveringToolbar = () => {
  const ref = useRef<HTMLDivElement>(null);
  const editor = useSlate();
  const selectionCanHaveInline = useSelectionCanHaveInline();
  // TODO: need paragraph button first in order to be able to turn list into paragraph
  const selectionCanBecomeList = true;
  // const selectionCanBecomeList = useSelectionCanBecomeList();
  const canTurnToHeading = true;
  // TODO: when P button is available
  // const canTurnToHeading =
  //   !!editor.selection && canNodeBeHeading(firstElementAtSelection(editor));

  useEffect(() => {
    const el = ref.current;
    const { selection } = editor;

    if (!el) {
      return;
    }

    if (
      !selection ||
      !ReactEditor.isFocused(editor) ||
      // Range.isCollapsed(selection) ||
      Editor.string(editor, selection) === ''
    ) {
      el.removeAttribute('style');
      return;
    }

    const domSelection = window.getSelection();
    if (domSelection) {
      const domRange = domSelection.getRangeAt(0);
      const rect = domRange.getBoundingClientRect();
      el.style.opacity = '1';
      el.style.top = `${rect.top + window.pageYOffset - el.offsetHeight}px`;
      el.style.left = `${
        rect.left + window.pageXOffset - el.offsetWidth / 2 + rect.width / 2
      }px`;
    }
  });

  return (
    <Portal>
      <Menu
        ref={ref}
        css={{
          border: '1px solid white',
          padding: '8px 7px 6px',
          position: 'absolute',
          zIndex: 1,
          top: '-10000px',
          left: '-10000px',
          marginTop: '-6px',
          opacity: 0,
          backgroundColor: '#222',
          borderRadius: '4px',
          transition: 'opacity 0.75s'
        }}
      >
        <IconButtonGroup>
          <MarkButton dark mark="bold" icon={<TypeBold size={20} />} />
          <MarkButton dark mark="italic" icon={<TypeItalic size={20} />} />
          <MarkButton
            dark
            mark="underline"
            icon={<TypeUnderline size={20} />}
          />
        </IconButtonGroup>

        <VerticalSeparator dark />

        {canTurnToHeading && (
          <React.Fragment>
            <IconButtonGroup>
              <HeadingButton dark level={2} icon={<TypeH2 size={20} />} />
              <HeadingButton dark level={3} icon={<TypeH3 size={20} />} />
            </IconButtonGroup>
          </React.Fragment>
        )}

        {(selectionCanBecomeList || selectionCanHaveInline) && (
          <VerticalSeparator dark />
        )}

        <IconButtonGroup>
          {selectionCanBecomeList && (
            <React.Fragment>
              <ListButton dark ordered icon={<ListOl size={20} />} />
              <ListButton dark icon={<ListUl size={20} />} />
            </React.Fragment>
          )}

          {selectionCanHaveInline && (
            <React.Fragment>
              <VerticalSeparator dark />
              <LinkButton dark />
            </React.Fragment>
          )}
        </IconButtonGroup>
      </Menu>
    </Portal>
  );
};
