import { isListActive, toggleList } from '../elements';

import React from 'react';
import { ToolbarButton } from './buttons';
import { useSlate } from 'slate-react';
import { useSelectionCanBecomeList } from '../hooks';

export const ListButton: React.FC<{
  ordered?: boolean;
  dark?: boolean;
  icon: React.ReactElement;
}> = ({ dark, ordered = false, icon }) => {
  const editor = useSlate();
  // TODO: need paragraph button first in order to be able to turn list into paragraph
  // const disabled = !useSelectionCanBecomeList();

  return (
    <ToolbarButton
      dark={dark}
      // disabled={disabled}
      active={isListActive(editor, ordered)}
      onClick={() => toggleList(editor, ordered)}
      icon={icon}
    />
  );
};
