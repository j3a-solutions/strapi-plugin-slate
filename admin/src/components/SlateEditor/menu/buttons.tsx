/** @jsx jsx */

import { ReactEditor, useFocused, useSlate } from 'slate-react';
import { isMarkActive, toggleMark } from '../format';

import { Mark } from '../elements/types';
import React, { MouseEvent } from 'react';
import { jsx } from '@emotion/react';
import styled from '@emotion/styled';
import { useIsSelectionActive } from '../hooks';
import { Icon } from 'react-bootstrap-icons';

export const VerticalSeparator = styled.span(
  ({ dark }: { dark?: boolean }) => ({
    height: '30px',
    margin: '0 1rem',
    borderLeft: `1px solid ${dark ? '#aaaaaa' : '#E3E9F3'}`
  })
);

type ToolbarButtonProps = {
  dark?: boolean;
  active: boolean;
  disabled?: boolean;
  onClick: () => void;
  icon: React.ReactElement;
};

export const ToolbarButton: React.FC<ToolbarButtonProps> = ({
  active,
  disabled,
  dark,
  icon,
  onClick = () => {}
}) => {
  let color: string;
  const editorInFocus = useFocused();
  const editor = useSlate();
  const isSelectionActive = useIsSelectionActive();
  // also consider editor.selection as editor loses focus on clicking on toolbar
  const showDisabled = disabled || (!editorInFocus && !isSelectionActive);

  if (showDisabled) {
    color = '#d1d1d1';
  } else if (active) {
    color = dark ? 'white' : '#0b6eef';
  } else {
    color = dark ? '#aaaaaa' : '#515151';
  }

  const css: any = {
    padding: '0 8px',
    color
  };

  if (dark) {
    css.backgroundColor = 'inherit';
    css.borderLeft = 'none !important';
  }

  /**
   * NOTE: this issue is killing me: https://github.com/ianstormtaylor/slate/issues/3412
   * TLDR: Clicking on any menu button, losses Editable's focus, and with that `editor.selection` is gone
   * This means you can't query for current location, nodes, etc.
   *
   * There are several hacks in that thread:
   * - CSS: userSelect: none
   * - Use onMouseDown instead of onClick
   * - Keep state somewhere else (the most convoluted)
   *
   */
  return (
    <button
      css={css}
      type="button"
      disabled={disabled}
      // style={{ userSelect: 'none' }}
      onMouseDown={(e: MouseEvent) => {
        e.preventDefault();
        onClick();
      }}
    >
      {icon}
    </button>
  );
};

export const MarkButton: React.FC<{
  mark: Mark;
  dark?: boolean;
  icon: React.ReactElement;
}> = ({ dark = false, icon, mark }) => {
  const editor = useSlate();

  return (
    <ToolbarButton
      dark={dark}
      active={isMarkActive(editor, mark)}
      onClick={() => toggleMark(editor, mark)}
      icon={icon}
    />
  );
};
