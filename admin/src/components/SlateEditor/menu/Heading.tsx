import { HeadingElement } from '../elements/types';
import React from 'react';
import { ToolbarButton } from './buttons';
import { useSlate } from 'slate-react';
import { isHeadingActive, toggleHeading } from '../elements';

export const HeadingButton: React.FC<{
  level: HeadingElement['level'];
  icon: React.ReactElement;
  dark?: boolean;
}> = ({ dark, level, icon }) => {
  const editor = useSlate();
  // TODO: only when P button is available
  // const disabled =
  //   !!editor.selection && !canNodeBeHeading(firstElementAtSelection(editor));

  return (
    <ToolbarButton
      dark={dark}
      // disabled={disabled}
      active={isHeadingActive(editor, level)}
      onClick={() => toggleHeading(editor, level)}
      icon={icon}
    />
  );
};
