import { ToolbarButton } from './buttons';
import React from 'react';
import { useSlate } from 'slate-react';
import { selectionContains, ElementType } from '../elements';
import { Editor, Element as SlateElement, Transforms } from 'slate';

/**
 * Toggle a block without custom properties
 */
const toggleBlockQuote = (editor: Editor) => {
  const isActive = selectionContains(editor, ElementType.BlockQuote);

  const newProperties: Partial<SlateElement> = {
    t: isActive ? ElementType.Paragraph : ElementType.BlockQuote
  };

  Transforms.setNodes(editor, newProperties);
};

export const BlockQuoteButton: React.FC<{
  dark?: boolean;
  icon: React.ReactElement;
}> = ({ dark, icon }) => {
  const editor = useSlate();
  return (
    <ToolbarButton
      dark={dark}
      active={selectionContains(editor, ElementType.BlockQuote)}
      icon={icon}
      onClick={() => {
        toggleBlockQuote(editor);
      }}
    />
  );
};
