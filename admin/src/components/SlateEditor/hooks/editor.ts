import { useSlateSelector } from 'slate-react';
import { isListItemInSelection, selectionCanBecomeList } from '../elements';
import { isParagraphInSelection } from '../elements/paragraph';

export const useIsSelectionActive = (): boolean =>
  useSlateSelector(editor => Boolean(editor.selection));

export const useSelectionCanHaveInline = (): boolean =>
  useSlateSelector(
    editor => isParagraphInSelection(editor) || isListItemInSelection(editor)
  );

export const useSelectionCanBecomeList = (): boolean =>
  useSlateSelector(
    editor => !!editor.selection && selectionCanBecomeList(editor)
  );
