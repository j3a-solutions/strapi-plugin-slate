import { Editor, Element } from 'slate';
import { CustomElement, ElementType } from './types';

export const findElementInSelection = (editor: Editor, type: ElementType) => {
  const [match] = Editor.nodes(editor, {
    match: node => {
      return (
        !Editor.isEditor(node) && Element.isElement(node) && node.t === type
      );
    }
  });

  return match as CustomElement[] | undefined;
};

export const selectionContains = (editor: Editor, format: ElementType) =>
  !!findElementInSelection(editor, format);

/**
 * Get current element with guards
 * This function makes sure selection is not lost and uses right method
 */
export function getNodes(
  editor: Editor,
  opts: Parameters<typeof Editor.nodes>[1]
) {
  const { selection } = editor;
  if (!selection) {
    throw Error(
      'Editor has lost selection. Have you look at https://github.com/ianstormtaylor/slate/issues/3412? See menu button  notes'
    );
  }
  /**
   * NOTE: Editor.node(editor, selection)[0] and Editor.first(editor, selection) return a Node type with text prop only, instead of CustomElement
   */
  return Editor.nodes(editor, {
    at: selection,
    ...opts
  });
}

export function firstElementAtSelection(editor: Editor): CustomElement {
  const el = getNodes(editor, {
    match: node => !Editor.isEditor(node) && Element.isElement(node)
  }).next().value;

  if (!el) {
    throw Error('Unable to get first element in selection');
  }

  return el[0] as CustomElement;
}

export const selectedListItems = (editor: Editor) =>
  getNodes(editor, {
    match: node =>
      !Editor.isEditor(node) &&
      Element.isElement(node) &&
      node.t === ElementType.ListItem
  });
