import { Editor, Element, Node, Transforms } from 'slate';
import { ElementType, ListElement } from './types';
import { getNodes, selectionContains } from './queries';
import { paragraphsFromSelectedList } from './transformers';

export const isListActive = (editor: Editor, ordered: boolean) => {
  const [match] = Editor.nodes(editor, {
    match: n =>
      isListElement(n) &&
      selectionContains(editor, ElementType.List) &&
      n.ordered === ordered
  });

  return !!match;
};

export const isListElement = (element: Node): element is ListElement =>
  Element.isElement(element) && element.t === ElementType.List;

export const isListItemInSelection = (editor: Editor) =>
  selectionContains(editor, ElementType.ListItem);

const typesWhichCanBecomeListItem = [ElementType.Paragraph];

export function selectionCanBecomeList(editor: Editor): boolean {
  const notAllowedElements = getNodes(editor, {
    match: node =>
      !Editor.isEditor(node) &&
      Element.isElement(node) &&
      !typesWhichCanBecomeListItem.includes(node.t)
  }).next().value;

  return !notAllowedElements;
}

export const toggleList = (editor: Editor, ordered: boolean) => {
  /**
   * Turn all list items in selection into paragraphs
   * If no items are left, list will get removed
   */
  if (selectionContains(editor, ElementType.List)) {
    const [paragraphs, unsetKeys] = paragraphsFromSelectedList(editor);

    Transforms.unwrapNodes(editor, {
      match: isListElement,
      split: true
    });

    for (const p of paragraphs) {
      Transforms.unsetNodes(editor, unsetKeys);
      Transforms.setNodes(editor, p);
    }

    return;
  }

  if (selectionCanBecomeList(editor)) {
    // change to list item
    // Paragraph and List Item share same props
    Transforms.setNodes(editor, { t: ElementType.ListItem });

    // Wrap in a ListElement
    const block: ListElement = {
      t: ElementType.List,
      ordered,
      children: []
    };
    Transforms.wrapNodes(editor, block);
  }
};
