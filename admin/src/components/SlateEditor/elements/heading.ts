import { Editor, Element, Node, Transforms } from 'slate';
import { isParagraphInSelection, newParagraph } from './paragraph';
import { ElementType, HeadingElement } from '../elements/types';

import { firstElementAtSelection, selectionContains } from './queries';
import { paragraphFromHeading } from './transformers';

export const isHeadingActive = (editor: Editor, level: number) => {
  const [match] = Editor.nodes(editor, {
    match: n =>
      isHeadingElement(n) &&
      selectionContains(editor, ElementType.List) &&
      n.level === level
  });

  return !!match;
};

const isHeadingElement = (x: Node): x is HeadingElement =>
  Element.isElement(x) && x.t === ElementType.Heading;

const typesWhichCanBeHeading = [ElementType.Paragraph];

export const canNodeBeHeading = (node: Node) => {
  return (
    !Editor.isEditor(node) &&
    Element.isElement(node) &&
    typesWhichCanBeHeading.includes(node.t)
  );
};

export const toggleHeading = (
  editor: Editor,
  level: HeadingElement['level']
) => {
  const firstElement = firstElementAtSelection(editor);

  if (isHeadingElement(firstElement)) {
    /**
     * Change heading level
     */
    if (firstElement.level !== level) {
      return Transforms.setNodes(editor, { level });
    }

    /**
     * Remove heading
     */
    const [paragraph, unsetKeys] = paragraphFromHeading(firstElement);
    Transforms.unsetNodes(editor, unsetKeys);
    return Transforms.setNodes(editor, paragraph);
  }

  /**
   * Change from another element to Heading
   */
  if (canNodeBeHeading(firstElement)) {
    if (isParagraphInSelection(editor)) {
      return Transforms.setNodes(editor, { t: ElementType.Heading, level });
    } else {
      // NOTE: only paragraph allowed for now
      // if other types are required, un-setting needs happening
      throw Error('Only paragraphs are allowed to change into Heading');
    }
  }
};
