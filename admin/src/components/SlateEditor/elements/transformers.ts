import {
  CustomElement,
  ElementType,
  HeadingElement,
  ParagraphElement
} from './types';
import { Editor, Node } from 'slate';
import { selectedListItems } from './queries';

export type ElementTransformer<
  I extends CustomElement,
  O extends CustomElement
> = (el: I) => [el: O, unsetKeys: Array<keyof I>];

export const newParagraph = (text = ''): ParagraphElement => ({
  t: ElementType.Paragraph,
  children: [{ text }]
});

export const paragraphFromHeading: ElementTransformer<
  HeadingElement,
  ParagraphElement
> = el => [newParagraph(Node.string(el)), ['level']];

function paragraphsFromListItems(editor: Editor): ParagraphElement[] {
  const selectedItems = selectedListItems(editor);
  const output: ParagraphElement[] = [];

  for (const [item] of selectedItems) {
    output.push(newParagraph(Node.string(item)));
  }

  return output;
}

export const paragraphsFromSelectedList = (
  editor: Editor
): [ParagraphElement[], string[]] => [
  paragraphsFromListItems(editor),
  ['ordered']
];
