import { Editor } from 'slate';
import { ElementType, ParagraphElement } from './types';
import { selectionContains } from './queries';

export const isParagraphInSelection = (editor: Editor) =>
  selectionContains(editor, ElementType.Paragraph);

export const newParagraph = (text = ''): ParagraphElement => ({
  t: ElementType.Paragraph,
  children: [{ text }]
});

export const emptyParagraph = (): ParagraphElement => newParagraph();
