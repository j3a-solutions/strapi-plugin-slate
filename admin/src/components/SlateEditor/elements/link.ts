import { ElementType, ExternalLinkElement, InternalLinkElement } from './types';
import {
  Node,
  Element as SlateElement,
  Editor,
  Range,
  Transforms,
  NodeEntry
} from 'slate';
import { selectionContains } from './queries';

export const isExternalLink = (x: Node): x is ExternalLinkElement =>
  SlateElement.isElement(x) && x.t === ElementType.LinkExternal;

export function activeInternalLink(editor: Editor) {
  const [link] = Editor.nodes(editor, {
    match: n =>
      isInternalLink(n) && selectionContains(editor, ElementType.LinkInternal)
  });
  return link as unknown as NodeEntry<InternalLinkElement> | undefined;
}

export function activeExternalLink(editor: Editor) {
  const [link] = Editor.nodes(editor, {
    match: n =>
      isExternalLink(n) && selectionContains(editor, ElementType.LinkExternal)
  });
  return link as unknown as NodeEntry<ExternalLinkElement> | undefined;
}

export const isInternalLink = (x: Node): x is ExternalLinkElement =>
  SlateElement.isElement(x) && x.t === ElementType.LinkInternal;

const isExternalLinkActive = (editor: Editor) => !!activeExternalLink(editor);

const isInternalLinkActive = (editor: Editor) => !!activeInternalLink(editor);

export const isLinkActive = (editor: Editor) =>
  isExternalLinkActive(editor) || isInternalLinkActive(editor);

export const isLinkElement = (
  element: Node
): element is ExternalLinkElement | InternalLinkElement =>
  isExternalLink(element) || isInternalLink(element);

/**
 * Take the text of the link and convert it into Paragraph
 * Slate will merge and flatten the Paragraphs if there is more than one with same style
 */
export const unwrapLink = (editor: Editor) =>
  Transforms.unwrapNodes(editor, {
    match: isLinkElement
  });

function insertLink(
  editor: Editor,
  link: ExternalLinkElement | InternalLinkElement
) {
  if (editor.selection) {
    const isCollapsed = Range.isCollapsed(editor.selection);

    if (isLinkActive(editor)) {
      /**
       * User has selected that contains a link
       * We need to "unwrap" it (i.e.: take the text out of the link and add it to the paragraph)
       * Otherwise, the new link will get inserted as child of the active link, which is not even valid HTML
       */
      unwrapLink(editor);
    }

    if (isCollapsed) {
      /**
       * Editor is active, but user has selected no text
       */
      Transforms.insertNodes(editor, link);
    } else {
      // Wrap selected text with the link
      // Result: ExternalLinkElement is inserted where the text is and the selected text becomes its child
      // It may split, as the selected text may have to be "extracted" from a paragraph
      Transforms.wrapNodes(editor, link, { split: true });
      Transforms.collapse(editor, { edge: 'end' });
    }
  }
}

export function insertExternalLink(editor: Editor, url: string) {
  if (editor.selection) {
    const isCollapsed = Range.isCollapsed(editor.selection);

    const link: ExternalLinkElement = {
      t: ElementType.LinkExternal,
      url,
      children: isCollapsed ? [{ text: url }] : []
    };

    insertLink(editor, link);
  }
}

export function insertInternalLink(
  editor: Editor,
  data: Omit<InternalLinkElement, 't' | 'children'>
) {
  if (editor.selection) {
    const isCollapsed = Range.isCollapsed(editor.selection);

    const link: InternalLinkElement = {
      ...data,
      t: ElementType.LinkInternal,
      children: isCollapsed ? [{ text: data.label }] : []
    };

    insertLink(editor, link);
  }
}
