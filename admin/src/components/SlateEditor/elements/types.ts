import type { BaseEditor } from 'slate';
import type { HistoryEditor } from 'slate-history';
import type { ReactEditor } from 'slate-react';

export type CustomEditor = BaseEditor & ReactEditor & HistoryEditor;

export enum ElementType {
  Paragraph,
  Heading,
  BlockQuote,
  LinkExternal,
  LinkInternal,
  List,
  ListItem
}

export type FormattedText = {
  text: string;
  bold?: boolean;
  italic?: boolean;
  underline?: boolean;
};

export type ExternalLinkElement = {
  t: ElementType.LinkExternal;
  url: string;
  children: CustomText[];
};

export type InternalLinkElement = {
  t: ElementType.LinkInternal;
  // Serial id of strapi content. Not named `id` to disambiguate with Gatsby's node id
  contentId: number;
  // Content name is the content-type name
  contentName: string;
  children: CustomText[];
  // endpoint: string;
  // label is what is shown to the User in strapi when hovering
  label: string;
};

export type InlineNode =
  | FormattedText
  | ExternalLinkElement
  | InternalLinkElement;

export type ParagraphElement = {
  t: ElementType.Paragraph;
  children: InlineNode[];
};

export type HeadingElement = {
  t: ElementType.Heading;
  level: 2 | 3;
  children: CustomText[];
};

export type BlockQuoteElement = {
  t: ElementType.BlockQuote;
  children: CustomText[];
};

export type ListElement = {
  t: ElementType.List;
  ordered: boolean;
  children: ListItemElement[];
};

export type ListItemElement = {
  t: ElementType.ListItem;
  children: InlineNode[];
};

export type BlockNode =
  | ParagraphElement
  | HeadingElement
  | BlockQuoteElement
  | ListElement
  | ListItemElement;

export type CustomElement =
  | BlockNode
  | ExternalLinkElement
  | InternalLinkElement;

export type Mark = keyof Omit<FormattedText, 'text'>;

export type CustomText = FormattedText;

declare module 'slate' {
  interface CustomTypes {
    Editor: CustomEditor;
    Element: CustomElement;
    Text: CustomText;
  }
}
