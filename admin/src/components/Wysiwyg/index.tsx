/** @jsx jsx */

import Editor, { emptyParagraph } from '../SlateEditor';
import { Typography } from '@strapi/design-system';
import React from 'react';
import styled from '@emotion/styled';
import { jsx } from '@emotion/react';

const EMPTY_VALUE = [emptyParagraph()];

const WysiwygContainer = styled.div({
  marginBottom: '4rem'
});

const RequiredSymbol = () => (
  <span css={{ color: '#d02b20', fontSize: '0.875rem', lineHeight: 1.43 }}>
    *
  </span>
);

type Props = {
  errors?: any[];
  inputDescription?:
    | string
    | Function
    | {
        id: string;
        params: Record<string, any>;
      }
    | null;
  intlLabel: {
    id: string;
    defaultMessage: string;
  };
  name: string;
  noErrorsDescription: boolean;
  onChange: (value: { target: { name: string; value: string } }) => void;
  required: boolean;
  value: string;
};

const Wysiwyg: React.FC<Props> = React.memo(
  ({ errors = [], name, value, onChange, intlLabel, required }) => {
    if (errors.length) {
      console.error('Some error in plugin: ');
      console.info('Now you now how they look!! TODO: Show errors in UI');
    }

    const handleChange = (value: any) => {
      // if (data.mime.includes('image')) {
      //   const imgTag = `<p><img src="${data.url}" caption="${data.caption}" alt="${data.alternativeText}"></img></p>`;
      //   const newValue = value ? `${value}${imgTag}` : imgTag;

      //   onChange({ target: { name, value: newValue } });
      // }

      // console.log('received value from editor: ', value);
      onChange({ target: { name, value: JSON.stringify(value) } });

      // Handle videos and other type of files by adding some code
    };

    return (
      <WysiwygContainer>
        <Typography
          fontWeight="bold"
          variant="pi"
          as="label"
          css={{ display: 'block', marginBottom: '1rem' }}
        >
          {intlLabel.defaultMessage}
          {required && <RequiredSymbol />}
        </Typography>
        <Editor
          initialValue={value ? JSON.parse(value) : EMPTY_VALUE}
          onChange={handleChange}
        />
        {/* TODO: this is where errors used to be shown in v3 */}
      </WysiwygContainer>
    );
  },
  (prev, next) => {
    /**
     * Stop re-rendering except when value changes from null to string
     */
    const hasAlreadyLoadedTruthyInitialValue = !!prev.value;
    const hasJustLoadedValue = !prev.value && !!next.value;
    return hasAlreadyLoadedTruthyInitialValue || !hasJustLoadedValue;
  }
);

export default Wysiwyg;
