import { prefixPluginTranslations } from '@strapi/helper-plugin';
const pluginPkg = require('../../package.json');
import pluginId from './pluginId';
import Initializer from './components/Initializer';
// import PluginIcon from './components/PluginIcon';
import Wysiwyg from './components/Wysiwyg';

const name = pluginPkg.strapi.name;

export default {
  register(app: any) {
    // app.addMenuLink({
    //   to: `/plugins/${pluginId}`,
    //   icon: PluginIcon,
    //   intlLabel: {
    //     id: `${pluginId}.plugin.name`,
    //     defaultMessage: name
    //   },
    //   Component: async () => {
    //     const component = await import(
    //       /* webpackChunkName: "[request]" */ './pages/App'
    //     );

    //     return component;
    //   },
    //   permissions: [
    //     // Uncomment to set the permissions of the plugin here
    //     // {
    //     //   action: '', // the action name should be plugin::plugin-name.actionType
    //     //   subject: null,
    //     // },
    //   ]
    // });
    const plugin = {
      id: pluginId,
      initializer: Initializer,
      isReady: false,
      name
    };

    app.registerPlugin(plugin);
    app.addFields({ type: 'wysiwyg', Component: Wysiwyg });
  },

  bootstrap(app: any) {},
  async registerTrads(app: any) {
    const { locales } = app;

    const importedTrads = await Promise.all(
      locales.map((locale: string) => {
        return import(`./translations/${locale}.json`)
          .then(({ default: data }) => {
            return {
              data: prefixPluginTranslations(data, pluginId),
              locale
            };
          })
          .catch(() => {
            return {
              data: {},
              locale
            };
          });
      })
    );

    return Promise.resolve(importedTrads);
  }
};
